# Introduction

PixVU is a simple image viewer that I wrote while testing [GraalVM](https://graalvm.org) 
together with [JavaFX](https://openjfx.io/). The results worked better than I expected
so I started using it regularly

# The Good

* Can be used with regular folder or .cbz files.
* Support for animated images like GIFs. This is something I couldn't find in other cbz 
viewers.

# The Bad

* Keybindings are a joke, mostly because I haven't made up my mind on they should be.
Will be improved though.
* Toolbar is terrible too, expecially the icons.
* Also the toolbar doesn't seem to properly work right now with the binary build. Running as a regular java programs work though
# Requirements

* [GraalVM](https://graalvm.org)  with native-image installed. I used 22.0.0.2, but anything
more recent should work, and most probably anything older too. You will need the Java 17
version tough, some features not available in Java 11 are being used
* A version of [Maven](https://maven.apache.org/) that supports Java 17

# Building

mvn gluonfx:build

This will create an executable in ./target/gluonfx/<arch>/ named PixVU, for example
./target/gluonfx/x86_64-linux/PixVU.


# Running

You can run the binary build with the previous step. The executable doesn't need any
JDK or JRE to run, but given the amount of libraries that it links, it is not exactly portable.

Another alternative, useful expecially during development, is to run the program without
using native-image, by running something like _./mvnw compile exec:java "-Dexec.arguments=..."_.
The provided _run.sh_ script in the main folder does exactly that.
