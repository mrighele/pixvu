package it.righele.pixvu;

import it.righele.pixvu.walker.WalkerEntry;
import javafx.event.Event;
import javafx.event.EventType;

import java.nio.file.Path;
import java.nio.file.WatchEvent;

public class ImageChangeEvent extends Event {
    public static EventType<ImageChangeEvent> IMAGE_CHANGED = new EventType<>(Event.ANY, "IMAGE_CHANGED");
    private WalkerEntry image;
    private WatchEvent.Kind<Path> kind;


    public ImageChangeEvent(WalkerEntry image, WatchEvent.Kind<Path> kind) {
        super(IMAGE_CHANGED);
        this.image = image;
        this.kind = kind;
    }

    public WalkerEntry getImage() {
        return image;
    }

    public void setImage(WalkerEntry image) {
        this.image = image;
    }

    public WatchEvent.Kind<Path> getKind() {
        return kind;
    }

    public void setKind(WatchEvent.Kind<Path> kind) {
        this.kind = kind;
    }
}
