package it.righele.pixvu;

import it.righele.pixvu.walker.FileWalker;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;

import javax.imageio.ImageIO;
import java.awt.datatransfer.DataFlavor;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.logging.Logger;

public class ImageComponent extends ScrollPane {

    private static final Logger logger = Logger.getLogger(ImageComponent.class.getName());

    private final ImageView imageView;

    private final BooleanProperty shrinkWidth = new SimpleBooleanProperty(true);
    private final BooleanProperty shrinkHeight = new SimpleBooleanProperty(true);
    private final BooleanProperty expandWidth = new SimpleBooleanProperty(true);
    private final BooleanProperty expandHeight = new SimpleBooleanProperty(true);

    private final ObjectProperty<Image> selectedImage = new SimpleObjectProperty<>();


    private void displayImage(ImageView imageView, Image image) {
//        logger.fine("pane size " + this.getLayoutBounds().toString());
//        logger.fine("view size " + imageView.getLayoutBounds().toString());
        resizeImage(imageView, image);

    }

    private void resizeImage(ImageView imageView, Image image) {
        if (image == null) {
            return;
        }
        var bounds = getLayoutBounds();
        imageView.setImage(image);
        var resizeWidth = (shrinkWidth.get() && image.getWidth() > bounds.getWidth() - 16) ||
                (expandWidth.get() && image.getWidth() < bounds.getWidth() - 16);
        if (resizeWidth) {
//            logger.fine("Fitting image width to " + bounds.getWidth());
            imageView.setFitWidth(bounds.getWidth() - 16);
        } else {
            imageView.setFitWidth(0);
        }

        var resizeHeight = (shrinkHeight.get() && image.getHeight() > bounds.getHeight() - 16) ||
                (expandHeight.get() && image.getHeight() < bounds.getHeight() - 16);
        if (resizeHeight) {
//            logger.fine("Fitting image height to " + bounds.getHeight());
            imageView.setFitHeight(bounds.getHeight() - 15);
        } else {
            imageView.setFitHeight(0);
        }

    }

    private final static DataFormat dataFormat = new DataFormat("image/png");

    private void setupDragAndDrop(ImageView imageView) {
            imageView.setOnDragDetected(event -> {
                try {
                    Dragboard db = imageView.startDragAndDrop(TransferMode.COPY);
                    ClipboardContent clipboardContent = new ClipboardContent();
                    clipboardContent.putImage(selectedImage.get());
                    db.setContent(clipboardContent);
                    event.consume();
                } catch (Exception e) {
                }

            });

    }

    public ImageComponent(FileWalker walker) {
        imageView = new ImageView();
        imageView.setPreserveRatio(true);

        selectedImage.addListener((observable, oldValue, newValue) -> {
            displayImage(imageView, newValue);
        });

        var centererStack = new StackPane(imageView);
        setContent(centererStack);

        layoutBoundsProperty().addListener((observable, oldValue, newValue) -> {
            if (Math.abs(oldValue.getWidth() - newValue.getWidth()) <= 1 &&
                    Math.abs(oldValue.getHeight() - newValue.getHeight()) <= 1) {
                // Minimal change, ignore it
                return;
            }
            logger.fine("pane size changed to " + newValue);
            resizeImage(imageView, imageView.getImage());
        });
        setFitToWidth(true);
        setFitToHeight(true);

        ChangeListener<Boolean> resizer = (observable, oldValue, newValue) -> resizeImage(imageView, imageView.getImage());
        expandWidth.addListener(resizer);
        shrinkWidth.addListener(resizer);
        expandHeight.addListener(resizer);
        shrinkHeight.addListener(resizer);
        setupDragAndDrop(imageView);
    }


    public boolean isShrinkWidth() {
        return shrinkWidth.get();
    }

    public BooleanProperty shrinkWidthProperty() {
        return shrinkWidth;
    }

    public void setShrinkWidth(boolean shrinkWidth) {
        this.shrinkWidth.set(shrinkWidth);
    }

    public boolean isShrinkHeight() {
        return shrinkHeight.get();
    }

    public BooleanProperty shrinkHeightProperty() {
        return shrinkHeight;
    }

    public void setShrinkHeight(boolean shrinkHeight) {
        this.shrinkHeight.set(shrinkHeight);
    }

    public boolean isExpandWidth() {
        return expandWidth.get();
    }

    public BooleanProperty expandWidthProperty() {
        return expandWidth;
    }

    public void setExpandWidth(boolean expandWidth) {
        this.expandWidth.set(expandWidth);
    }

    public boolean isExpandHeight() {
        return expandHeight.get();
    }

    public BooleanProperty expandHeightProperty() {
        return expandHeight;
    }

    public void setExpandHeight(boolean expandHeight) {
        this.expandHeight.set(expandHeight);
    }

    public Image getSelectedImage() {
        return selectedImage.get();
    }

    public ObjectProperty<Image> selectedImageProperty() {
        return selectedImage;
    }

    public void setSelectedImage(Image selectedImage) {
        this.selectedImage.set(selectedImage);
    }
}
