package it.righele.pixvu;

import it.righele.pixvu.walker.FileWalker;
import it.righele.pixvu.walker.WalkerEntry;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.scene.image.Image;
import org.apache.commons.collections4.map.LRUMap;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * A class that stores most used images in the current folder, which means
 * - The first image
 * - The last image
 * - The current image
 * - The next image
 * - The previous image
 */
public class ImageLoader {
    private static final Logger logger = Logger.getLogger(ImageLoader.class.getName());

    private static final ExecutorService executor = Executors.newFixedThreadPool(4);
    private FileWalker fileWalker;

    private ObjectProperty<Image> currentImage = new SimpleObjectProperty<>();

    private LRUMap<WalkerEntry, Task<Image>> imageLRUMap = new LRUMap<WalkerEntry, Task<Image>>(10, 10) {
        @Override
        protected boolean removeLRU(final LinkEntry<WalkerEntry, Task<Image>> entry) {
            boolean removed = super.removeLRU(entry);
            if (removed) {
                Task<Image> task = entry.getValue();
                if (task != null) {
                    task.cancel();
                }
            }
            return removed;
        }
    };

    private Image loadWebpImage(WalkerEntry entry) {
        logger.fine("File is a webp, converting first");
        try {
            BufferedImage webp = ImageIO.read(entry.getInputStream());
            Path tmpPath = Files.createTempFile("", ".png");
            logger.fine("temporary file is:" + tmpPath.toString());
            ImageOutputStream output = ImageIO.createImageOutputStream(  tmpPath.toFile());

            ImageIO.write(webp, "PNG", output);
            // FIXME: Delete the temporary image
            return new Image(tmpPath.toUri().toString());
        } catch (IOException e) {
            logger.warning("Could not load webp: " + e.getMessage());
            return null;
        }
    }

    private Image loadImage(WalkerEntry entry)  {
        if (entry != null) {
            if (entry.toString().endsWith(".webp")) {
                return loadWebpImage(entry);
            } else {
                try {
                    return new Image(entry.getInputStream());
                } catch( IOException e ) {
                    logger.warning("Could not load image: " + e.getMessage());
                    return null;
                }
            }
        }
        return null;

    }



    private Task<Image> loadIfNeeded(WalkerEntry entry) {
        Task<Image> task;
        if (!imageLRUMap.containsKey(entry)) {
            task = new Task<Image>() {
                @Override
                protected Image call() throws Exception {
                    Thread.sleep(50);
                    if (Thread.interrupted()) {
                        logger.fine("Interrupted");
                        return null;
                    }
                    return  loadImage(entry);
                }
            };
            executor.submit(task);
            imageLRUMap.put(entry, task);
            logger.fine("Image not found in LRU, queue load: " + entry.getFilename());
        } else {
            task = imageLRUMap.get(entry);
            logger.fine("Returning image found in LRU: " + entry.getFilename());
        }
        return task;
    }

    private Task<Image> currentTask;

    public ImageLoader(FileWalker walker) {
        fileWalker = walker;

        fileWalker.currentFileProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null) {
                logger.fine("Current file changed from " + oldValue.toString() + " to " + newValue.toString());
            }

            Task<Image> task = loadIfNeeded(newValue);
            currentTask = task;
            if (task.isDone()) {
                currentImage.set(task.getValue());
            } else {
                task.setOnSucceeded(event -> {
                    // Maybe we need on the application thread
                    if (!task.isCancelled() &&   task == currentTask) {
                        logger.fine("Image loaded for " + newValue.toString() + ": " + task.getValue());
                        currentImage.set(task.getValue());
                    }
                });
            }
            if (walker.getPosition() == 0) {
                logger.fine("Moving to the beginning");
                loadIfNeeded(walker.getNextFile());
            } else if (walker.getPosition() == walker.getnFiles() - 1) {
                loadIfNeeded(walker.getPreviousFile());
            } else if (oldValue.equals(walker.getNextFile())) {
                logger.fine("Moving backwards");
                loadIfNeeded(walker.getPreviousFile());
            } else if (oldValue.equals(walker.getPreviousFile())) {
                logger.fine("Moving forward");
                loadIfNeeded(walker.getNextFile());
            }

        });
    }

    public Image getCurrentImage() {
        return currentImage.get();
    }

    public ObjectProperty<Image> currentImageProperty() {
        return currentImage;
    }

    public void setCurrentImage(Image currentImage) {
        this.currentImage.set(currentImage);
    }
}
