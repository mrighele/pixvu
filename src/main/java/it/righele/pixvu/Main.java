package it.righele.pixvu;

import it.righele.pixvu.controller.MainController;
import it.righele.pixvu.walker.FileWalker;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    private void setupInitialSize(Stage stage) {
        // Get the primary screen size
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        // Set the initial size of the primary stage to half the screen size
        stage.setWidth(screenBounds.getWidth() / 2);
        stage.setHeight(screenBounds.getHeight() / 2);
    }

    private Scene getScene() throws IOException {
        FileWalker walker = FileWalker.getFileWalker(getParameters());
        FileWalker thumbnailWalker = FileWalker.getFileWalker(getParameters());

        var controller = new MainController(walker, thumbnailWalker);
        var root = controller.getRoot();
        Scene scene = new Scene(root);
        scene.setOnKeyPressed(event -> {
            logger.fine("Key pressed: " + event.getCode());
            switch (event.getCode()) {
                case PAGE_UP -> walker.goBack();
                case LEFT -> walker.goBack();
                case PAGE_DOWN -> walker.goForward();
                case RIGHT -> walker.goForward();
                case HOME -> walker.firstFile();
                case END -> walker.lastFile();
                case DELETE -> walker.deleteCurrent();
            }
        });
        root.layout();
        walker.firstFile();
        return scene;

    }

    @Override
    public void start(Stage stage) throws Exception {
        LogManager.getLogManager().readConfiguration(Main.class.getClassLoader().getResourceAsStream("logging.properties"));
        setupInitialSize(stage);
        Scene scene = getScene();

        stage.setTitle("PixVU");
        stage.setScene(scene);
        stage.setOnCloseRequest(windowEvent -> {
            Platform.exit();
            System.exit(0);
        });
        stage.show();

    }


    /**
     * The main() method is ignored in correctly deployed JavaFX application. main() serves only as fallback in case the
     * application can not be launched through deployment artifacts, e.g., in IDEs with limited FX support. NetBeans
     * ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
