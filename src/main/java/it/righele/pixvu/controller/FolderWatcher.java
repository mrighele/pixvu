package it.righele.pixvu.controller;

import it.righele.pixvu.walker.FileWalker;
import it.righele.pixvu.ImageChangeEvent;
import it.righele.pixvu.walker.GenericFileWalker;
import javafx.event.EventHandler;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Class to centralize watchers
 * When we open a folder we probably have two walkers watching it, the one for the images
 * and the one for the thumbnails, and we don't want to register a watcher two times since it
 * seems to cause some issues.
 * <p>
 *  FIXME: Yes, I know, this class doesn't handle cleanup of listeners, but for now it is fine since we
 *  always open a single folder. If we plan to be able to change folders, this should be fixed.
 */
public class FolderWatcher {
    private static Logger logger = Logger.getLogger(FolderWatcher.class.getName());

    private static Map<String, List<EventHandler<ImageChangeEvent>>> eventHandlers = new HashMap<>();

    public static void addEventHandler(Path path, EventHandler<ImageChangeEvent> handler) throws IOException {
        String key = path.toAbsolutePath().toString();
        if (!eventHandlers.containsKey(key)) {
            logger.fine("Watcher for folder " + key + " doesn't exists, I will set it up");
            setupWatcher(path);
        }
        eventHandlers.get(key).add(handler);
    }

    private static void notifyEventHandlers(String key, ImageChangeEvent event) {
        logger.fine("Event handlers: " + eventHandlers.size());
        for (var handler : eventHandlers.get(key)) {
            logger.fine("Notify event handler " + handler);
            handler.handle(event);
        }
    }

    /**
     * Start a filesystem watcher so that we be notified of changes to the files and update, for example, the thumbnail accordingly
     *
     * @param path
     * @throws IOException
     */
    private synchronized static void setupWatcher(Path path) throws IOException {
        String folder = path.toAbsolutePath().toString();
        eventHandlers.put(folder, new ArrayList<>());
        WatchService watcher = FileSystems.getDefault().newWatchService();
        path.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.OVERFLOW);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                // Start an infinite loop to process events
                while (true) {
                    logger.fine("File system event received");
                    // Wait for a watch key
                    WatchKey key = watcher.take();

                    // Iterate over the events in the watch key
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();
                        Path eventPath = Paths.get(path.toString(), ((Path) event.context()).toString());
                        // Print the kind of event
                        if (kind == StandardWatchEventKinds.ENTRY_CREATE && Files.isRegularFile(eventPath) && FileWalker.isImage(eventPath.toString())) {
                            logger.fine("Notify image creation event");
                            notifyEventHandlers(folder, new ImageChangeEvent( new GenericFileWalker.FolderWalkerEntry( eventPath), (WatchEvent.Kind<Path>) kind));
                        } else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
                            logger.fine("Notify image deletion event");
                            notifyEventHandlers(folder, new ImageChangeEvent(new GenericFileWalker.FolderWalkerEntry( eventPath), (WatchEvent.Kind<Path>) kind));
                        }
                    }

                    // Reset the watch key
                    boolean valid = key.reset();
                    if (!valid) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                // Handle interrupted exception
            }
        });

    }

}
