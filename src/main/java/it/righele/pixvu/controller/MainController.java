package it.righele.pixvu.controller;

import it.righele.pixvu.walker.FileWalker;
import it.righele.pixvu.ImageComponent;
import it.righele.pixvu.ImageLoader;
import it.righele.pixvu.thumbnail.ThumbnailComponent;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.materialdesign2.MaterialDesignA;
import org.kordamp.ikonli.materialdesign2.MaterialDesignC;

import java.io.IOException;
import java.util.logging.Logger;

public class MainController {
    private static final Logger logger = Logger.getLogger(MainController.class.getName());

    private final Pane root;
    private ImageComponent imageComponent;
    private ImageLoader imageLoader;

    private ToolBar setupToolbar(FileWalker walker) {
        Button nextImage = new Button("", new FontIcon(MaterialDesignC.CHEVRON_RIGHT));
        nextImage.setOnAction(actionEvent -> walker.goForward());

        Button previousImage = new Button("", new FontIcon(MaterialDesignC.CHEVRON_LEFT));
        previousImage.setOnAction(actionEvent -> walker.goBack());

        ToggleButton shrinkWidthButton = new ToggleButton("", new FontIcon(MaterialDesignA.ARROW_COLLAPSE_HORIZONTAL));
        Bindings.bindBidirectional(shrinkWidthButton.selectedProperty(), imageComponent.shrinkWidthProperty());

        ToggleButton expandWidthButton = new ToggleButton("", new FontIcon(MaterialDesignA.ARROW_EXPAND_HORIZONTAL));
        Bindings.bindBidirectional(expandWidthButton.selectedProperty(), imageComponent.expandWidthProperty());

        ToggleButton shrinkHeightButton = new ToggleButton("", new FontIcon(MaterialDesignA.ARROW_COLLAPSE_VERTICAL));
        Bindings.bindBidirectional(shrinkHeightButton.selectedProperty(), imageComponent.shrinkHeightProperty());

        ToggleButton expandHeightButton = new ToggleButton("", new FontIcon(MaterialDesignA.ARROW_EXPAND_VERTICAL));
        Bindings.bindBidirectional(expandHeightButton.selectedProperty(), imageComponent.expandHeightProperty());


        return new ToolBar(previousImage, nextImage, shrinkWidthButton, expandWidthButton, shrinkHeightButton, expandHeightButton);
    }

    private HBox setupStatusbar(FileWalker walker) {
        logger.fine("Setting up status bar");
        Label currentFile = new Label();

        Label resolution = new Label();
        Label itemNumber = new Label();
        walker.currentFileProperty().addListener((observable, oldValue, newValue) -> {
            currentFile.setText(newValue.getFilename());
            var pos = (walker.getPosition() + 1) + "/" + walker.getnFiles();
            itemNumber.setText(pos);
        });

        walker.nFilesProperty().addListener((observable, oldValue, newValue) -> {
            var pos = (walker.getPosition() + 1) + "/" + walker.getnFiles();
            itemNumber.setText(pos);
        });


        imageLoader.currentImageProperty().addListener((observable, oldValue, newValue) -> {
            newValue.widthProperty().addListener((observable1, oldWidth, width) -> {
                if (newValue.getWidth() > 0 && newValue.getHeight() > 0 && newValue == imageLoader.getCurrentImage()) {
                    var res = ((int) newValue.getWidth()) + "x" + ((int) newValue.getHeight());
                    resolution.setText(res);
                }
            });
            newValue.heightProperty().addListener((observable1, oldWidth, width) -> {
                if (newValue.getWidth() > 0 && newValue.getHeight() > 0 && newValue == imageLoader.getCurrentImage()) {
                    var res = ((int) newValue.getWidth()) + "x" + ((int) newValue.getHeight());
                    resolution.setText(res);
                }
            });
            if (newValue.getWidth() > 0 && newValue.getHeight() > 0) {
                var res = ((int) newValue.getWidth()) + "x" + ((int) newValue.getHeight());
                resolution.setText(res);
            }

        });

        var spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);


        HBox box = new HBox(itemNumber, new Separator(Orientation.VERTICAL), resolution, new Separator(Orientation.VERTICAL), spacer, currentFile);

        var margin = new Insets(2, 10, 2, 10);
        HBox.setMargin(itemNumber, margin);
        HBox.setMargin(resolution, margin);
        HBox.setMargin(spacer, margin);
        HBox.setMargin(currentFile, margin);
        currentFile.setAlignment(Pos.CENTER_RIGHT);
        return box;
    }


    private HBox setupMainArea(FileWalker walker, FileWalker thumbnailWalker) throws IOException {
        var thumbnails = new ThumbnailComponent(thumbnailWalker);

        imageComponent = new ImageComponent(walker);
        imageLoader = new ImageLoader(walker);
        imageLoader.currentImageProperty().bindBidirectional(imageComponent.selectedImageProperty());
        HBox box = new HBox(thumbnails, imageComponent);
        HBox.setHgrow(imageComponent, Priority.ALWAYS);
        box.setAlignment(Pos.CENTER);
        box.setFillHeight(true);
        thumbnails.selectedFileProperty().bindBidirectional(walker.currentFileProperty());
        return box;
    }

    private Pane setupPage(FileWalker walker, FileWalker thumbnailWalker) throws IOException {

        var mainArea = setupMainArea(walker, thumbnailWalker);
        var toolbar = setupToolbar(walker);
        var statusbar = setupStatusbar(walker);
        var result = new VBox(toolbar, mainArea, statusbar);
        VBox.setVgrow(mainArea, Priority.ALWAYS);
        return result;
    }

    public MainController(FileWalker walker, FileWalker thumbnailWalker) throws IOException {
        logger.fine("Creating page");
        root = setupPage(walker, thumbnailWalker);
        logger.fine("Displaying first image");
    }


    public Parent getRoot() {
        return root;
    }
}
