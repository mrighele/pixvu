package it.righele.pixvu.walker;

import it.righele.pixvu.ImageChangeEvent;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.event.EventHandler;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

public interface FileWalker {

    static Set<String> imageExtensions = Set.of("jpeg", "jpg", "png", "gif", "webp");

    static Path getSelectedPath(Application.Parameters parameters) {
        for (var param : parameters.getRaw()) {
            return Paths.get(param);
        }
        return Paths.get(System.getProperty("user.dir"));
    }

    static FileWalker getFileWalker(Application.Parameters parameters) throws IOException {
        Path path = getSelectedPath(parameters);
        if (path.toFile().isDirectory()) {
            return new GenericFileWalker(path);
        } else if (path.toFile().getPath().endsWith(".zip") || path.toFile().getPath().endsWith(".cbz")) {
            return new ZipFileWalker(path);
        }
        return new GenericFileWalker(getSelectedPath(parameters));

    }

    static boolean isImage(String name) {
        if (name.indexOf(".") < 0) {
            return false;
        }
        String[] parts = name.toLowerCase().split("\\.");
        return imageExtensions.contains(parts[parts.length - 1]);
    }


    void firstFile();

    void lastFile();

    void goForward();

    void goBack();

    WalkerEntry getCurrentFile();

    WalkerEntry getPreviousFile();

    WalkerEntry getNextFile();

    ObjectProperty<WalkerEntry> currentFileProperty();

    int getPosition();

    int getnFiles();

    IntegerProperty nFilesProperty();


    void addEventHandler(EventHandler<ImageChangeEvent> handler) throws IOException;

    void deleteCurrent();

    /**
     * Return the root folder. Could be the path to a zip file for example, or
     * to a folder containing the images
     * @return
     */
    Path getRoot();
}
