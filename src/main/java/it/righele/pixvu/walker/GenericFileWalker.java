package it.righele.pixvu.walker;

import it.righele.pixvu.ImageChangeEvent;
import it.righele.pixvu.controller.FolderWatcher;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class GenericFileWalker implements FileWalker {
    public static class FolderWalkerEntry implements it.righele.pixvu.walker.WalkerEntry, Comparable {
        private Path path;

        public FolderWalkerEntry(Path path) {
            this.path = path;
        }

        @Override
        public String getFilename() {
            return path.toString();
        }

        @Override
        public InputStream getInputStream() throws FileNotFoundException {
            return new FileInputStream(path.toFile());
        }

        @Override
        public int compareTo(Object o) {
            return this.getFilename().compareTo(((WalkerEntry)o).getFilename());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FolderWalkerEntry that = (FolderWalkerEntry) o;
            return Objects.equals(path, that.path);
        }

        @Override
        public int hashCode() {
            return Objects.hash(path);
        }
    }

    private Path path;
    private static Logger logger = Logger.getLogger(GenericFileWalker.class.getName());
    private int position;
    private List<WalkerEntry> contents = new ArrayList<>();

    private ObjectProperty<it.righele.pixvu.walker.WalkerEntry> currentFile = new SimpleObjectProperty<>();

    private IntegerProperty nFiles = new SimpleIntegerProperty();

    public GenericFileWalker(Path path) throws IOException {
        this.path = path;
        logger.fine(path.toUri().toString());
        Iterable<Path> roots;
        int depth;
        if (Files.isDirectory(path)) {
            roots = Arrays.asList(path);
            depth = 1;
        } else {
            roots = FileSystems.newFileSystem(path, Collections.emptyMap())
                    .getRootDirectories();
            depth = 2000;
        }
        for (Path root : roots) {
            Files.walk(root, depth, FileVisitOption.FOLLOW_LINKS)
                    .filter(Files::isRegularFile)
                    .filter(f -> FileWalker.isImage(f.toString()   ))
                    .forEach(p -> contents.add(new FolderWalkerEntry(p)));
        }
        logger.fine("Got " + contents.size() + " files");
        contents = contents.stream().sorted().collect(Collectors.toList());
        nFiles.set(contents.size());
        this.currentFile.addListener((observable, oldValue, newValue) -> {
            position = contents.indexOf(newValue);
        });
        FolderWatcher.addEventHandler(path,event -> {
            if (event.getKind() == StandardWatchEventKinds.ENTRY_DELETE ) {
                String p = event.getImage().getFilename();
                logger.fine("Deleting " + p + " to walker");
                final int removedPosition = contents.indexOf(event.getImage());
                if (removedPosition < 0) {
                    logger.warning("Deleted element not found ");
                    return;
                }
                logger.fine("Item position was " + removedPosition + " and the current one is " + this.position);
                boolean refresh = removedPosition == this.position;
                contents.remove(event.getImage());
                if (removedPosition <= this.position) {
                    this.position -= 1;
                }

                Platform.runLater(() -> {
                    logger.fine("Cleaning up: updating folder size to " + contents.size());
                    nFiles.set(contents.size());
                    if (refresh) {
                        logger.fine("The image shown has been removed, update it");
                        goBack();
                        goForward();
                    }
                });
            } else if (event.getKind() == StandardWatchEventKinds.ENTRY_CREATE) {
                WalkerEntry entry = event.getImage();
                logger.fine("Adding " + entry.getFilename() + " to walker");
                contents.add( entry );
                Platform.runLater(() -> {
                    nFiles.set(contents.size());
                });
            }
        });
    }

    @Override
    public void addEventHandler(EventHandler<ImageChangeEvent> handler) throws IOException {
        FolderWatcher.addEventHandler(path, handler);
    }

    @Override
    public void deleteCurrent() {
        String filename = currentFile.get().getFilename();
        logger.fine("Deleting file " + filename);
        File f = new File(filename);
        f.delete();
    }

    @Override
    public Path getRoot() {
        return this.path.toAbsolutePath();
    }


    @Override
    public void firstFile() {
        position = 0;
        currentFile.set(contents.get(position));
    }

    @Override
    public void lastFile() {
        position = contents.size() - 1;
        currentFile.set(contents.get(position));

    }

    @Override
    public void goForward() {
        position = Math.min(position + 1, contents.size() - 1);
        currentFile.set(contents.get(position));

    }

    @Override
    public void goBack() {
        position = Math.max(position - 1, 0);
        currentFile.set(contents.get(position));

    }

    private void refreshPosition() {
        currentFile.set(contents.get(position));
    }

    @Override
    public it.righele.pixvu.walker.WalkerEntry getCurrentFile() {
        return currentFile.get();
    }

    @Override
    public it.righele.pixvu.walker.WalkerEntry getPreviousFile() {
        int pos = Math.max(0, position - 1);
        return contents.get(pos);
    }


    @Override
    public it.righele.pixvu.walker.WalkerEntry getNextFile() {
        int pos = Math.min(contents.size() - 1, position + 1);
        return contents.get(pos);
    }

    @Override
    public ObjectProperty<it.righele.pixvu.walker.WalkerEntry> currentFileProperty() {
        return currentFile;
    }


    @Override
    public int getPosition() {
        return position;
    }

    public int getnFiles() {
        return nFiles.get();
    }

    public IntegerProperty nFilesProperty() {
        return nFiles;
    }
}
