package it.righele.pixvu.walker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public interface WalkerEntry {
    String getFilename();
    InputStream getInputStream() throws IOException;

}
