package it.righele.pixvu.walker;

import it.righele.pixvu.ImageChangeEvent;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipFileWalker implements FileWalker {
    /**
     * Path to the zip file
     */
    private Path path;

    private static Logger logger = Logger.getLogger(GenericFileWalker.class.getName());

    private class ZipWalkerEntry implements WalkerEntry {
        ZipEntry entry;

        @Override
        public String getFilename() {
            return entry.getName();
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return zipFile.getInputStream(entry);
        }

        public ZipWalkerEntry(ZipEntry entry) {
            this.entry = entry;
        }

    }

    private ZipFile zipFile;
    private List<ZipWalkerEntry> entries = new ArrayList<>();

    private int position = -1;

    private IntegerProperty nFiles = new SimpleIntegerProperty();

    private ObjectProperty<WalkerEntry> currentFile = new SimpleObjectProperty<>();

    public ZipFileWalker(Path path) throws IOException {
        zipFile = new ZipFile(path.toFile());
        zipFile.stream().filter(e -> FileWalker.isImage(e.getName())).forEach(e -> entries.add(new ZipWalkerEntry(e)));
        nFiles.set(entries.size());
        this.currentFile.addListener((observable, oldValue, newValue) -> {
            position = entries.indexOf(newValue);
        });

        this.path = path;


    }

    @Override
    public void firstFile() {
        if (position != 0) {
            position = 0;
            currentFile.set(entries.get(position));
        }
    }

    @Override
    public void lastFile() {
        if (position != entries.size() - 1) {
            position = entries.size() - 1;
            currentFile.set(entries.get(position));

        }
    }

    @Override
    public void goForward() {
        position += 1;
        if (position > entries.size() - 1) {
            position = entries.size() - 1;
        } else {
            currentFile.set(entries.get(position));
        }

    }

    @Override
    public void goBack() {
        position -= 1;
        if (position < 0) {
            position = 0;
        } else {
            currentFile.set(entries.get(position));
        }
    }

    @Override
    public WalkerEntry getCurrentFile() {
        return entries.get(position);
    }

    @Override
    public WalkerEntry getPreviousFile() {
        int pos = position > 0 ? position - 1 : position;
        return entries.get(pos);
    }

    @Override
    public WalkerEntry getNextFile() {
        int pos = position == entries.size() - 1 ? position : position + 1;
        return entries.get(pos);
    }

    @Override
    public ObjectProperty<WalkerEntry> currentFileProperty() {
        return currentFile;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public int getnFiles() {
        return nFiles.get();
    }

    @Override
    public IntegerProperty nFilesProperty() {
        return nFiles;
    }

    @Override
    public void addEventHandler(EventHandler<ImageChangeEvent> handler) throws IOException {
        // We don't handle events, so ignore this.
    }

    @Override
    public void deleteCurrent() {
        // Ignore the operation, for now
        logger.fine("Ignoring delete operation");
    }

    @Override
    public Path getRoot() {
        return this.path.toAbsolutePath();
    }
}
