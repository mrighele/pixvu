module it.righele.pixvu {
    requires javafx.controls;
    requires javafx.graphics;
    requires java.desktop;
//    requires javafx.fxml;
    requires java.base;
    requires java.logging;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.materialdesign2;
    requires org.apache.commons.collections4;
}